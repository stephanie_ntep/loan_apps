# loan_apps

## User Story

 User melakukan registrasi dengan data diri, email, nomor telepon dan upload foto
beserta KTP
 User dapat login dengan password atau biometric (jika ada di perangkat mobilenya)
 User dapat melihat Sisa hutang dan tagihan perbulan yang harus di bayarkan (Jika ada)
 User dapat meminjam uang paling besar Rp. 12.000.000 dengan tenor maksimal 1 taun.
 User dalam proses peminjaman akan di proses dengan hasil diterima atau ditolak
 Jika pinjaman diterima maka akan ada notifikasi lewat email dan nomot telepon yang
terdaftar
 User tidak dapat melakukan peminjaman uang jika sedang ada proses peminjaman dan
belum di lunaskan.

1. Buatlah high level design architecture atas project mobile apps ini.
2. Spesifikasikan design Screen Flow dan ERD atas rancangan yang ingin anda buat.
3. Buatlah detail design untuk API dengan menggunakan tools design seperti UML,
   ERD,flowchart etc.
4. Buatlah detail design untuk screen behavior dari mobile apps berdasarkan screen flow
   diatas.

## HLD - ERD - API Design

https://drive.google.com/file/d/1IHErlxMyhONLIWvZnTdSqOJ_TPQznVWp/view?usp=sharing

## Screen Behavior

https://www.figma.com/file/IGGR47fHNn4QyQPemlotvy/Mandiri?type=design&node-id=0%3A1&mode=design&t=b3AU0UF1GEsgGEd2-1
